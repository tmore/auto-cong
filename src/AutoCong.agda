module AutoCong where



open import AutoCong.All using ( rewriteOnce
                               ; rewriteOnceBackwards
                               ; rewriteListOnce
                               ; rewriteAll
                               )
                               public
open import AutoCong.Nth using (rewriteNth) public

open import AutoCong.Search using ( rewriteSearch
                                  ; rewriteSearchN
                                  ; rewriteSearchIncremental
                                  ) public
