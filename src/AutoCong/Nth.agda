{-
Author: Tomas Möre

Module that defines the 'autoCongAll' macro. Used to automatically construct
cong terms to rewrite expressions. This version replaces all occurences within the goal.
-}
module AutoCong.Nth where

open import AutoCong.Utils.Abstraction

open import AutoCong.Utils.Utils
open import AutoCong.Utils.Terms

open import Relation.Binary.PropositionalEquality using (cong ; trans)
open import Relation.Nullary.Decidable as Dec

open import Data.Unit using (⊤ ; tt)
open import Data.Bool using (Bool ; true ; false ; if_then_else_ )

open import Agda.Builtin.Equality

open import Agda.Builtin.Reflection

open import Data.List using (List ; [] ; _∷_)
open import Reflection.TypeChecking.MonadSyntax
open import Reflection.Term using (_≟_)

open import Agda.Builtin.Nat

open import Function
open import Data.Product
open import Data.Maybe using (Maybe ; just ; nothing)

makeCongTermForwards : Term → Type → Type → Nat → TC (Maybe Term)
makeCongTermForwards proof
             (def (quote _≡_) (_ ∷ _ ∷ (arg _ thmLHS) ∷ (arg _ thmRHS) ∷ []))
             (def (quote _≡_) (_ ∷ _ ∷ (arg _ holeLHS) ∷ (arg _ holeRHS) ∷ [])) n =
  returnTC
    (Data.Maybe.map
      (λ lambdaTerm → cong-term lambdaTerm proof)
      (abstractOnNth n thmLHS holeLHS))
makeCongTermForwards _ thmType (def (quote _≡_) _) _ =
  typeError (strErr "First argument to makeCongTermForwards must be an equality but got:\n "
            ∷ termErr thmType
            ∷ [])
makeCongTermForwards _ _ goalType _ =
  typeError (strErr "makeCongTermForwards must be placed where an equality is explected but got:\n"
            ∷ termErr goalType
            ∷ [])


macro
  -- Constructs a call to 'cong' using an equailty proof and the goal.  This
  -- version replaces the nth occurence of the left hand side of the given
  -- equality With the right hand side.
  rewriteNth : Nat → Term → Term → TC ⊤
  rewriteNth n thm hole = do
    thmType <- inferType thm
    holeType <- inferType hole
    congTerm <- makeCongTermForwards thm thmType holeType n
    unify hole (Data.Maybe.maybe id refl-term congTerm)
