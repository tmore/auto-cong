{-
Author: Tomas Möre 2020

This module contains some utitilies that does not fit anywhere else

-}
module AutoCong.Utils.Utils where


open import Relation.Nullary.Decidable using (isYes)

open import Agda.Builtin.Reflection
open import Agda.Builtin.Equality
open import Function

import Data.List as List


open import Agda.Builtin.List using ( List ; _∷_ ; [] )
open import Data.List using ([_])

open import Data.Bool using (Bool ; true ; false)
open import Data.Maybe using (Maybe ; just ; nothing)
open import Data.Empty
open import Data.Unit

open import Data.Product

open import Reflection.Term as Term

-- mapArg(s) are a sligthly less verbose way to map over the Arg type
mapArg : {A B : Set} → (A → B) → Arg A → Arg B
mapArg f (arg argInfo x) = arg argInfo (f x)

mapArgs : {A B : Set} → (A → B) → List (Arg A) → List (Arg B)
mapArgs = Data.List.map ∘ mapArg

unArg : {A : Set} → Arg A → A
unArg (arg _ a) = a



Is≡ : Term → Set
Is≡ t@(def (quote _≡_) (a ∷ b ∷ c ∷ d ∷ [])) = ⊤
Is≡ _ = ⊥
ensureIs≡ : Term → Maybe (Σ Term Is≡)
ensureIs≡ t@(def (quote _≡_) (_ ∷ _ ∷ _ ∷ _ ∷ [])) =
  just (t , tt)
ensureIs≡ _ = nothing



-- Common term functions, thse are not made to be pretty, but to be quick and
-- easy to use.



-- quick and dirty way to get the lhs of an equivalence term
≡-lhs : Term → TC Term
≡-lhs (def (quote _≡_) (_ ∷ _ ∷ (arg _ lhs) ∷ _ ∷ [])) =
  returnTC lhs
≡-lhs _ =
  typeError [ strErr "Internal error: atempted to retrieve lhs of non '≡' term" ]

≡-rhs : Term → TC Term
≡-rhs (def (quote _≡_) (_ ∷ _ ∷ _ ∷ (arg _ rhs) ∷ [])) =
  returnTC rhs
≡-rhs _ =
  typeError [ strErr "Internal error: atempted to retrieve lhs of non '≡' term" ]

≡-lhs×rhs : Term → TC (Type × Type)
≡-lhs×rhs term = do
  termType <- inferType term
  lhs ← ≡-lhs termType
  rhs ← ≡-rhs termType
  returnTC (lhs , rhs)
  where
    open import Reflection using (_>>=_ ; _>>_)

≡-proof-list : List Term → TC (List (Type × (Type × Type)))
≡-proof-list [] = returnTC []
≡-proof-list (x ∷ xs) = do
  lhs-rhs ← ≡-lhs×rhs x
  rest ← ≡-proof-list xs
  returnTC ((x , lhs-rhs) ∷ rest)
  where
    open import Reflection using (_>>=_ ; _>>_)

≡-replace-lhs : Term → Term → TC Term
≡-replace-lhs newLhs (def (quote _≡_) (a1 ∷ a2 ∷ (arg v lhs) ∷ rhs ∷ [])) =
  returnTC (def (quote _≡_) (a1 ∷ a2 ∷ (arg v newLhs) ∷ rhs ∷ []))
≡-replace-lhs _ _ = typeError [ strErr "Internal error: Atempted to replace lhs of non ≡ term"]

≡-replace-rhs : Term → Term → TC Term
≡-replace-rhs newRhs (def (quote _≡_) (a1 ∷ a2 ∷ lhs ∷ (arg v rhs) ∷ [])) =
  returnTC (def (quote _≡_) (a1 ∷ a2 ∷ lhs ∷ (arg v newRhs)  ∷ []))
≡-replace-rhs _ _ = typeError [ strErr "Internal error: Atempted to replace rhs of non ≡ term"]

-- Retruns true if the type is the trivial equivalence type (lhs syntactically same as rhs)
isTrivial≡ : Type → Bool
isTrivial≡ (def (quote _≡_) (_ ∷ _ ∷ (arg _ lhs) ∷ (arg _ rhs) ∷ [])) =
  isYes (lhs Term.≟ rhs)
isTrivial≡ _ = false



-- Parses a term representing a list of terms to a list of terms.
listTerm→termList : Term → TC (List Term)
listTerm→termList (con (quote Agda.Builtin.List.List.[]) _) = returnTC []
listTerm→termList (con (quote _∷_) ((arg _ t) ∷ (arg _ tail) ∷ [])) =
  bindTC (listTerm→termList tail) (returnTC ∘ (t ∷_))
listTerm→termList nonList =
  typeError ( strErr "Error: Explected List but got:\n"
            ∷ termErr nonList
            ∷ [])
