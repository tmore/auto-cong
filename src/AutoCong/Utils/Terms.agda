{-
Author: Tomas Möre 2020
Email: tomas.o.more@gmail.com

This module contains utitites to generate commonly used terms.

-}

module AutoCong.Utils.Terms where


open import Agda.Builtin.Reflection

open import Data.Unit using (⊤ ; tt)


open import Agda.Builtin.Equality

open import Data.List using (List ; _∷_ ; [])


open import Relation.Binary.PropositionalEquality using (cong ; trans)


-- 'default' arginfo (visible and relevant)
vr-arg-info : ArgInfo
vr-arg-info = arg-info visible relevant

-- defualt argument type
vr-arg : {A : Set} → A → Arg A
vr-arg = arg vr-arg-info

-- Simple 'refl' term
refl-term : Term
refl-term = con (quote refl) []


-- Gives a call to 'cong'
cong-term : Term → Term → Term
cong-term a b =
  def (quote cong)
      (vr-arg a
      ∷ vr-arg b
      ∷ [])

-- Gives a call to 'trans'
trans-term : Term → Term → Term
trans-term a b =
  def (quote trans) (vr-arg a ∷ vr-arg b ∷ [])
