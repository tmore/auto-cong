{-
Author: Tomas Möre 2020

This module contains some utilities for working with abstractions in Agda
reflection.

-}

module AutoCong.Utils.Abstraction where

open import AutoCong.Utils.Utils
open import AutoCong.Utils.Terms

open import Relation.Nullary.Decidable as Dec

open import Data.Bool using (Bool ; true ; false ; if_then_else_ ; _∨_ )
open import Data.List using (List ; [] ; _∷_ ; _++_ ; [_])
open import Data.Product

open import Function
open import Agda.Builtin.Nat
import Data.Nat as Nat
import Data.Nat.Show as Nat
open import Agda.Builtin.Reflection

open import Data.Maybe using (Maybe ; just ; nothing )

open import Reflection.Term using (_≟_)
-- Increses the bruijn indexing by one where required when adding lambda
-- abstraction over a term. This method is internal. For actual usage see
-- 'lambdaIncreaseBruijnIdx'
--
-- In short, Every time a variable is encoutered the bruijn index is increased
-- if its value is greater then or equal to the number of lambdas encountered in
-- the descent of the syntax tree.
{-# TERMINATING #-}
lambdaIncreaseBruijnIdxRec : Nat → Term → Term
lambdaIncreaseBruijnIdxRec numLambdas (var x args) =
  if isYes (x Nat.≥? numLambdas)
  then var (suc x) newArgs
  else var x newArgs
  where
    newArgs = mapArgs (lambdaIncreaseBruijnIdxRec numLambdas) args
lambdaIncreaseBruijnIdxRec numLambdas (con n args) =
  con n args
  where
    newArgs = mapArgs (lambdaIncreaseBruijnIdxRec numLambdas) args
lambdaIncreaseBruijnIdxRec numLambdas (def n args) =
  def n args
  where
    newArgs = mapArgs (lambdaIncreaseBruijnIdxRec numLambdas) args

lambdaIncreaseBruijnIdxRec numLambdas (lam v (abs vname t)) =
  lam v (abs vname (lambdaIncreaseBruijnIdxRec (suc numLambdas) t))
lambdaIncreaseBruijnIdxRec numLambdas (pat-lam clauses args) =
  pat-lam fixedClauses fixedArgs
  where
    fixedArgs =
      mapArgs (lambdaIncreaseBruijnIdxRec numLambdas)
              args
    fixedClauses =
      Data.List.map
        (λ { (clause argPatterns t) →
                     clause argPatterns (lambdaIncreaseBruijnIdxRec numLambdas t)
           ; (absurd-clause argPatterns) → absurd-clause argPatterns
           })
        clauses
lambdaIncreaseBruijnIdxRec numLambdas (pi (arg info t) (abs n t')) =
  pi (arg info (lambdaIncreaseBruijnIdxRec numLambdas t))
     (abs n (lambdaIncreaseBruijnIdxRec (suc numLambdas) t'))
lambdaIncreaseBruijnIdxRec numLambdas (agda-sort (set t)) =
  agda-sort (set (lambdaIncreaseBruijnIdxRec numLambdas t))
lambdaIncreaseBruijnIdxRec numLambdas (agda-sort other) = agda-sort other
lambdaIncreaseBruijnIdxRec numLambdas (lit l) = lit l
lambdaIncreaseBruijnIdxRec numLambdas (meta m args) =
  meta m (mapArgs (lambdaIncreaseBruijnIdxRec numLambdas) args)
       -- Tbh, I dont know if I should touch meta variables or not. Leaving this
       -- for later
       --
       -- meta x (mapArgs (lambdaIncreaseBruijnIdxRec numLambdas) args)
lambdaIncreaseBruijnIdxRec numLambdas uknown = uknown

-- Wrapper function for lambdaIncreaseBruijnIdxRec. The version to be used
-- publicly
lambdaIncreaseBruijnIdx : Term → Term
lambdaIncreaseBruijnIdx =
  lambdaIncreaseBruijnIdxRec zero

-- Really an traverseable instance, but since we only write this once there's no
-- need to abstract
-- abstractOnMap : {A B : Set} → (A → (A × Bool)) → List A → (List B × Bool)
-- abstractOnMap f [] = (false , [])
-- abstractOnMap f (x ∷ xs) =
--   let (foundInX , x') = f x
--       (foundInRest , xs') = abstractOnMap f xs
--   in foundInX ∨ foundInRest , x' ∷ xs'

module _ where
  private
    _<*>_ : {A B : Set} → ((A → B) × Bool) → (A × Bool) → (B × Bool)
    _<*>_ (f , b₁) (x , b₂) = f x , (b₁ ∨ b₂)
    pure : {A : Set} → A → (A × Bool)
    pure a = a , false

    pureFound : {A : Set} → A → (A × Bool)
    pureFound a = (a , true)

    traverse : {A B : Set} → (A → (B × Bool)) → List A → (List B × Bool)
    traverse f [] = pure []
    traverse f (x ∷ xs) = ⦇ (f x) ∷ traverse f xs ⦈
    traverseArgs : {A B : Set} → (A → (B × Bool)) → List (Arg A) → (List (Arg B) × Bool)
    traverseArgs f = traverse (λ { (arg ai t) → map₁ (arg ai) (f t)})
-- Recursively replaces all occurences of the first term in the last term with a
-- variable such that it may be lambda abstracted. Returns a pair of a term and
-- a boolean value. If the process managed to find some occurence the value is
-- true, otherwise, false.
  {-# TERMINATING #-}
  abstractOnReplace : Nat → Term → Term → (Term × Bool)
  abstractOnRecur : Nat → Term → Term → (Term × Bool)


  abstractOnReplace bruijnIdx target term =
    if isYes (term ≟ target)
    then pureFound (var bruijnIdx [])
    else abstractOnRecur bruijnIdx target term


  abstractOnRecur bruijnIdx target (var x args) =
    ⦇ (var x) (traverseArgs (abstractOnReplace bruijnIdx target) args) ⦈
  abstractOnRecur bruijnIdx target (con n args) =
    ⦇ (con n) (traverseArgs (abstractOnReplace bruijnIdx target) args) ⦈
  abstractOnRecur bruijnIdx target (def n args) =
    ⦇ (def n) (traverseArgs (abstractOnReplace bruijnIdx target) args) ⦈
  abstractOnRecur bruijnIdx target (lam v (abs n t)) =
    ⦇ (lam v ∘ (abs n)) (abstractOnReplace (suc bruijnIdx)
                                           (lambdaIncreaseBruijnIdx target)
                                           t)
    ⦈
  abstractOnRecur bruijnIdx target (pat-lam clauses args) =
    ⦇ pat-lam (traverse (λ { (clause params t) →
                                     ⦇ (clause params) (abstractOnReplace bruijnIdx target t) ⦈
                           ; (absurd-clause params) →
                                     pure (absurd-clause params)
                           })
                        clauses)
              (traverseArgs (abstractOnReplace bruijnIdx target) args)
     ⦈
  abstractOnRecur bruijnIdx target (pi (arg argInfo t) (abs n t')) =
    ⦇ pi (map₁ (arg argInfo) (abstractOnRecur bruijnIdx target t))
         (map₁ (abs n) (abstractOnReplace (suc bruijnIdx)
                                          (lambdaIncreaseBruijnIdx target)
                                          t))
    ⦈
  abstractOnRecur bruijnIdx target (agda-sort  (set t)) =
    ⦇ (agda-sort ∘ set) (abstractOnReplace bruijnIdx target t) ⦈
  abstractOnRecur bruijnIdx target (agda-sort  (lit n)) =
    pure (agda-sort (lit n))
  abstractOnRecur bruijnIdx target (agda-sort  unknown) =
    pure (agda-sort unknown)
  abstractOnRecur bruijnIdx target (lit l) =
    pure (lit l)
  abstractOnRecur bruijnIdx target term@(meta m args) =
    ⦇ (meta m) (traverseArgs (abstractOnReplace bruijnIdx target) args) ⦈
  abstractOnRecur bruijnIdx target unknown =
    pure unknown

  -- Given a 'target' term and a term creates a lambda abstraction on all
  -- elements of the term equal to the target. Returns a pair of term and bool
  -- where the term is the lambda abstraction and the bool is true if any
  -- replacement took place.
  abstractOn : Term → Term → Maybe Term
  abstractOn target term =
    let (term , found) =
              abstractOnReplace
                zero
                target
                (lambdaIncreaseBruijnIdx term)
    in if found
       then just (lam visible (abs "z" term))
       else nothing

-- Recursively replaces all occurences of the first term in the last term with a
-- variable such that it may be lambda abstracted.
{-# TERMINATING #-}
abstractOnNthReplace : Nat → Term → Term → Nat → (Term × Bool × Nat)
abstractOnNthRecur : Nat → Term → Term → Nat → (Term × Bool × Nat)

nthMapArg : (Term → Nat → (Term × Bool × Nat)) → Arg Term → Nat → (Arg Term × Bool × Nat)

nthMapArgs : (Term → Nat → (Term × Bool × Nat)) → List (Arg Term) → Nat → (List (Arg Term) × Bool × Nat)

nthMap : {A : Set} → (A → Nat → (A × Bool × Nat)) → List A → Nat → (List A × Bool × Nat)
nthMap f [] n = ([] , false , n)
nthMap f (x ∷ xs) n
  with f x n
...| y , true , n' = y ∷ xs , true  , n'
...| y , false , n' =
       let (rest , done , n'' ) = nthMap f xs n'
       in y ∷ rest , done , n''


nthMapArg f (arg i t) n = Data.Product.map₁ (arg i) (f t n)
nthMapArgs f args = nthMap (nthMapArg f) args

abstractOnNthReplace bruijnIdx target term 0 =
  if isYes (term ≟ target)
  then (var bruijnIdx [] , true , 0 )
  else abstractOnNthRecur bruijnIdx target term 0
abstractOnNthReplace bruijnIdx target term (suc n) =
  if isYes (term ≟ target)
  then (term , false , n)
  else abstractOnNthRecur bruijnIdx target term (suc n)

abstractOnNthRecur bruijnIdx target (var x args) n =
  Data.Product.map₁ (var x) (nthMapArgs (abstractOnNthReplace bruijnIdx target) args n)
abstractOnNthRecur bruijnIdx target (con na args) n =
  Data.Product.map₁ (con na) (nthMapArgs (abstractOnNthReplace bruijnIdx target) args n)
abstractOnNthRecur bruijnIdx target (def na args) n =
  Data.Product.map₁ (def na) (nthMapArgs (abstractOnNthReplace bruijnIdx target) args n)
abstractOnNthRecur bruijnIdx target (lam v (abs x t)) n =
  Data.Product.map₁ (lam v ∘ (abs x)) (abstractOnNthReplace (suc bruijnIdx)  (lambdaIncreaseBruijnIdx target) t n)
abstractOnNthRecur bruijnIdx target (pat-lam clauses args) n =
  let (clauses' , done , n') =
                (nthMap (λ { (clause params t) n' →
                                       Data.Product.map₁ (clause params) (abstractOnNthReplace bruijnIdx target t n')
                            ; (absurd-clause params) n' →
                                      (absurd-clause params , false , n')
                            })
                         clauses
                         n)
  in if done
     then (pat-lam clauses' args , done , n')
     else Data.Product.map₁ (pat-lam clauses') (nthMapArgs (abstractOnNthReplace bruijnIdx target) args n')

abstractOnNthRecur bruijnIdx target (pi (arg argInfo t1) (abs na t2)) n
  with abstractOnNthRecur bruijnIdx target t1 n
...| t1' , true , n' = (pi (arg argInfo t1') (abs na t2) , true , n')
...| t1' , false , n' =
  Data.Product.map₁ (pi (arg argInfo t1') ∘ abs na)
                    (abstractOnNthReplace (suc bruijnIdx)
                                          (lambdaIncreaseBruijnIdx target)
                                          t2
                                          n')
abstractOnNthRecur bruijnIdx target (agda-sort (set t)) n =
  Data.Product.map₁ (agda-sort ∘ set) (abstractOnNthReplace bruijnIdx target t n)
abstractOnNthRecur bruijnIdx target (agda-sort  (lit l)) n =
  (agda-sort  (lit l) , false , n)
abstractOnNthRecur bruijnIdx target (agda-sort unknown) n =
  (agda-sort unknown , false , n)
abstractOnNthRecur bruijnIdx target (lit l) n =
  (lit l , false , n)
abstractOnNthRecur bruijnIdx target term@(meta m args) n =
  Data.Product.map₁ (meta m) (nthMapArgs (abstractOnNthReplace bruijnIdx target) args n)
abstractOnNthRecur bruijnIdx target unknown n =
  (unknown , false , n )

-- Given a number n, a 'target' term and a term creates a lambda abstraction on
-- on the nth'th occurence of the target in the term. Returns a pair where the
-- first element is the abstracted value and the second is a bool that is true
-- if it managed to replace something.
--
-- The occurences enumbered in tree pre-order (syntactially top to bottom)
abstractOnNth : Nat → Term → Term → Maybe Term
abstractOnNth nth target term =
  let (term' , didReplace , _) =
             abstractOnNthReplace 0 target (lambdaIncreaseBruijnIdx term) nth
  in if didReplace
     then just (lam visible (abs "z" term'))
     else nothing





-- Unsafely beta reduces a lambda term one step. Acts like 'id' in case the lhs
-- is not a an element that may take arguments. Marked terminating for
-- simplicity not correctness.
--
-- It is assumed that the input is well formed
{-# TERMINATING #-}
betaReduce : Term → Term → Term
betaReduceList : Term → List (Arg Term) → Term
betaReduceList term [] = term
betaReduceList term@(lam _ (abs _ _)) ((arg _ x) ∷ xs) =
  betaReduceList (betaReduce term x) xs
betaReduceList (var varIdx varArgs) args =
  var varIdx (varArgs ++ args)
betaReduceList (con conName conArgs) args =
  con conName (conArgs ++ args)
betaReduceList (def defName defArgs) args =
  def defName (defArgs ++ args)
betaReduceList (pat-lam clauses patArgs) args =
  pat-lam clauses (patArgs ++ args)
betaReduceList t _ = t -- All other cases are invalid but will not yield an
                       -- error. It is ass

betaReduce (lam _ (abs _ body)) x =
  substitute 0 body
  where
    substitute : Nat → Term → Term
    substitute i (var varIdx args) =
      if  (i Nat.≡ᵇ varIdx)
      then (case x of
            λ { (var xIdx xargs) →
                     var (xIdx + i)
                         (mapArgs (lambdaIncreaseBruijnIdxRec i) xargs
                         ++ mapArgs (substitute i) args)
              ; (con xName xargs) →
                     con xName (mapArgs (lambdaIncreaseBruijnIdxRec i) xargs
                               ++ mapArgs (substitute i) args)
              ; (def xName xargs) →
                     def xName (mapArgs (lambdaIncreaseBruijnIdxRec i) xargs
                               ++ mapArgs (substitute i) args)
              ; st@(lam xVisibility (abs _ subTerm)) →
                     betaReduceList st (mapArgs (substitute i) args)
              ; (pat-lam clauses xargs) →
                     pat-lam clauses (mapArgs (lambdaIncreaseBruijnIdxRec i) xargs
                                     ++ mapArgs (substitute i) args)
              ; t → t
              })
      else
        var (if isYes (varIdx Nat.≥? i)
             then varIdx - 1
             else varIdx)
            args
    substitute i (con conName args) =
      con conName (mapArgs (substitute i) args)
    substitute i (def defName args) =
      def defName (mapArgs (substitute i) args)
    substitute i (lam v (abs absName body)) =
      lam v (abs absName (substitute (suc i) body))
    substitute i (pat-lam defName args) =
      pat-lam defName (mapArgs (substitute i) args)
    substitute i (pi a (abs varName body)) =
      pi (mapArg (substitute i) a) (abs varName (substitute (suc i) body))
    substitute i (agda-sort (set t)) =
      agda-sort (set (substitute i t))
    substitute _ t@(agda-sort (lit _)) = t
    substitute _ t@(agda-sort unknown) = t
    substitute _ t@(lit _) = t
    substitute i (meta m args) =
      meta m (mapArgs (substitute i) args)
    substitute _ unknown = unknown
betaReduce t x = betaReduceList t [ vr-arg x ]
