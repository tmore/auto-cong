{-
Author: Tomas Möre 2020

Some examples of the features and essencially some flimsy unit tests

-}

module AutoCong.Examples where
open import Agda.Builtin.Equality
open import AutoCong.All
open import AutoCong.Nth
open import AutoCong.Search

open import Data.List using (List ; [] ; _∷_)

import Relation.Binary.PropositionalEquality as Eq
open Eq using (_≡_; refl; trans; sym; cong; cong-app; subst)
open Eq.≡-Reasoning -- using (begin_; _≡⟨⟩_; _≡⟨_⟩_; _∎)

postulate
  A : Set
  a : A
  b : A
  c : A
  op : A → A → A
  opr1 : op a b ≡ c
  opr2 : op c c ≡ a
  opr3 : op a c ≡ b


sillyEx1 : (op (op a b) (op a b)) ≡ (op c c)
sillyEx1 = rewriteOnce opr1


sillyEx1-0nth : (op (op a b) (op a b)) ≡ (op c (op a b))
sillyEx1-0nth = rewriteNth 0 opr1

sillyEx1-1nth : (op (op a b) (op a b)) ≡ (op (op a b) c)
sillyEx1-1nth = rewriteNth 1 opr1

nonAutoCongEx2 : op (op (op (op c c) (op a c)) (op a b)) c ≡ b
nonAutoCongEx2 =
  begin
    op (op (op (op c c) (op a c)) (op a b)) c
  ≡⟨ cong (λ z → op (op (op z (op a c)) (op a b)) c)  opr2 ⟩
    op (op (op a (op a c)) (op a b)) c
  ≡⟨ cong (λ z →  op (op (op a z) (op a b)) c) opr3 ⟩
    op (op (op a b) (op a b)) c
  ≡⟨ cong (λ z →  op (op z z) c) opr1  ⟩
    op (op c c) c
  ≡⟨ cong (λ z → op z c)  opr2 ⟩
    op a c
  ≡⟨ opr3 ⟩
    b
  ∎

sillyEx2 : op (op (op (op c c) (op a c)) (op a b)) c ≡ b
sillyEx2 =
  begin
    op (op (op (op c c) (op a c)) (op a b)) c
  ≡⟨ rewriteOnce opr2 ⟩
    op (op (op a (op a c)) (op a b)) c
  ≡⟨ rewriteOnce opr3 ⟩
    op (op (op a b) (op a b)) c
  ≡⟨ rewriteOnce opr1 ⟩
    op (op c c) c
  ≡⟨ rewriteOnce opr2 ⟩
    op a c
  ≡⟨ opr3 ⟩
    b
  ∎

sillyEx2-trans  : op (op (op (op c c) (op a c)) (op a b)) c ≡ b
sillyEx2-trans =
  trans (rewriteOnce opr2)
        (trans (rewriteOnce opr3)
               (trans (rewriteOnce opr1)
                      (trans (rewriteOnce opr2)
                             opr3)
                      ))

sillyEx2-trans-chain  : op (op (op (op c c) (op a c)) (op a b)) c ≡ b
sillyEx2-trans-chain =
  rewriteListOnce
    (  (quoteTerm opr2)
    ∷ (quoteTerm opr3)
    ∷ (quoteTerm opr1)
    ∷ (quoteTerm opr2)
    ∷ (quoteTerm opr3)
    ∷ [])

sillyEx2-trans-chain-All  : op (op (op (op c c) (op a c)) (op a b)) c ≡ b
sillyEx2-trans-chain-All =
  rewriteAll
    (  (quoteTerm opr2)
    ∷ (quoteTerm opr1)
    ∷ (quoteTerm opr3)
    ∷ [])

sillyEx3 : (op (op a b) (op a b)) ≡ a
sillyEx3 = rewriteAll ( quoteTerm  opr1 ∷ quoteTerm opr2 ∷ [])


backwardsTrivial : op a c ≡ b
backwardsTrivial = rewriteOnceBackwards opr3


backwardsExample : op (op (op (op c c) (op a c)) (op a b)) c ≡ op a c
backwardsExample =
  begin
    op (op (op (op c c) (op a c)) (op a b)) c
  ≡⟨ {!!} ⟩
    op (op c c) c
  ≡⟨ rewriteOnceBackwards opr2 ⟩
    op a c
  ∎




rewriteSearchEx1  : op (op (op (op c c) (op a c)) (op a b)) c ≡ b
rewriteSearchEx1 =
   rewriteSearch
    (  (quoteTerm opr2)
    ∷ (quoteTerm opr1)
    ∷ (quoteTerm opr3)
    ∷ [])


rewriteSearchEx2  : op (op (op (op c c) (op a c)) (op a b)) c ≡ b
rewriteSearchEx2 =
   rewriteSearch
    (  (quoteTerm opr2)
    ∷ (quoteTerm opr1)
    ∷ (quoteTerm opr3)
    ∷ [])


rewriteSearchEx3 : (op (op a b) (op a b)) ≡ (op c (op a b))
rewriteSearchEx3 =
  rewriteSearch
    ( (quoteTerm opr1)
    ∷ [])

rewriteSearchEx4 : (op (op a b) (op a b)) ≡ (op (op a b) c)
rewriteSearchEx4 =
 rewriteSearch
    ( (quoteTerm opr1)
    ∷ [])

rewriteSearchN-Ex1 : (op (op a b) (op a b)) ≡ (op (op a b) c)
rewriteSearchN-Ex1  =
 rewriteSearchN 3
    ( (quoteTerm opr1)
    ∷ [])

-- Same problem as 'rewriteSearchN-Ex1' but starts at a too low depth
rewriteSearchIncremental-Ex1 : (op (op a b) (op a b)) ≡ (op (op a b) c)
rewriteSearchIncremental-Ex1 =
  rewriteSearchIncremental 1 1 1000
    ( (quoteTerm opr1)
    ∷ [])
