{-
Author: Tomas Möre 2020
Email: tomas.o.more@gmail.com

Module that defines the 'autoCongAll' macro(S). Used to automatically construct
cong terms to rewrite expressions. This version replaces all occurences within the goal.

These functions could be optimized with a little bit of effort. May be optimized
in case somebody cares.

These functions are not defined to be safe or pretty.

Main macros are:

autoCongAll
autoCongAllBackwards
autoCongAllTrans

-}
module AutoCong.All where

open import AutoCong.Utils.Abstraction

open import AutoCong.Utils.Utils
open import AutoCong.Utils.Terms

open import Function

open import Relation.Binary.PropositionalEquality using (cong ; trans)
open import Relation.Nullary.Decidable as Dec

open import Data.Unit using (⊤ ; tt)
open import Data.Bool using (Bool ; true ; false ; if_then_else_ )

open import Agda.Builtin.Equality

open import Agda.Builtin.Reflection

open import Data.List using (List ; [] ; _∷_)
open import Reflection.TypeChecking.MonadSyntax
open import Reflection.Term using (_≟_)

open import Data.Product using (proj₁ ; proj₂ )

open import Data.Maybe using (Maybe ; just ; nothing)



-- Creates cong term using the argumetns. Fails with a type error if the term
-- arguments aren't of the equality kind.
--
-- May as well be replaced by "makeCongTransChainForwards"
makeCongTermForwards : Term → Type → Type → TC (Maybe Term)
makeCongTermForwards proof
             (def (quote _≡_) (_ ∷ _ ∷ (arg _ thmLHS) ∷ (arg _ thmRHS) ∷ []))
             (def (quote _≡_) (_ ∷ _ ∷ (arg _ holeLHS) ∷ (arg _ holeRHS) ∷ [])) =
  returnTC
    (Data.Maybe.map
      (λ lambdaTerm → cong-term lambdaTerm proof)
      (abstractOn thmLHS holeLHS))
makeCongTermForwards _ thmType (def (quote _≡_) _) =
  typeError (strErr "First argument to makeCongTermForwards must be an equality but got:\n "
            ∷ termErr thmType
            ∷ [])
makeCongTermForwards _ _ goalType =
  typeError (strErr "makeCongTermForwards must be placed where an equality is explected but got:\n"
            ∷ termErr goalType
            ∷ [])

makeCongTermBackwards : Term → Type → Type  → TC (Maybe Term)
makeCongTermBackwards proof
             (def (quote _≡_) (_ ∷ _ ∷ (arg _ thmLHS) ∷ (arg _ thmRHS) ∷ []))
             (def (quote _≡_) (_ ∷ _ ∷ (arg _ holeLHS) ∷ (arg _ holeRHS) ∷ [])) =
  returnTC
    (Data.Maybe.map
      (λ lambdaTerm → cong-term lambdaTerm proof)
      (abstractOn thmRHS holeRHS))
makeCongTermBackwards _ thmType (def (quote _≡_) _) =
  typeError (strErr "First argument to makeCongTermBackwards must be an equality but got:\n "
            ∷ termErr thmType
              ∷ [])
makeCongTermBackwards _ _ goalType =
  typeError (strErr "makeCongTermBackwards must be placed where an equality is explected but got:\n"
            ∷ termErr goalType
            ∷ [])

-- Given a list of equivalence proofs, a goal this function rewrites the term
-- using the proofs in order until it is either out of proofs or the goal has been reached.
makeCongTransChainForwards : List Term → Type → Term → TC Term
makeCongTransChainForwards [] goalType acc = returnTC acc
makeCongTransChainForwards (proof ∷ rest) goalType accTerm =
  do goalLhs <- ≡-lhs goalType
     goalRhs <- ≡-rhs goalType
     (if (isYes (goalLhs ≟ goalRhs))
      then returnTC accTerm
      else do
       proofType <- inferType proof
       mCongTerm <- makeCongTermForwards proof proofType goalType
       case mCongTerm of
         λ { nothing → makeCongTransChainForwards rest goalType accTerm
           ; (just congTerm) → do
               congType <- inferType congTerm
               congRhs <- ≡-rhs congType
               newGoalType <- ≡-replace-lhs congRhs goalType
               makeCongTransChainForwards
                 rest
                 newGoalType
                 (trans-term accTerm congTerm)
           })

-- Repeatedly calls makeCongTransChainForwards with the same proofs until it
-- either can't rewrite anymore or goal has been reached. May not terminate.
{-# TERMINATING #-}
repeatMakeCongTransChainForwards : List Term → Type → TC Term
repeatMakeCongTransChainForwards proofs originalGoal =
  do
    goalLHS ← ≡-lhs originalGoal
    origAccType ← ≡-replace-rhs goalLHS originalGoal
    rec originalGoal origAccType refl-term
  where
    rec : Type → Type → Term → TC Term
    rec goalType accType accTerm =
      if isTrivial≡ goalType
      then returnTC accTerm
      else
      do
        congTerm ← makeCongTransChainForwards proofs goalType accTerm
        congType ← inferType congTerm
        (if isYes (congType ≟ accType)
         then returnTC accTerm
         else
         do
           congRhs <- ≡-rhs congType
           newGoalType <- ≡-replace-lhs congRhs goalType
           rec newGoalType congType congTerm)



-- variant of

macro
  -- Constructs a call to 'cong' using an equailty proof and the goal.  This
  -- version replaces all occurenes of the left hand side of the given equality
  -- With the right hand side.
  rewriteOnce : Term → Term → TC ⊤
  rewriteOnce thm hole = do
    thmType <- inferType thm
    holeType <- inferType hole
    congTerm <- makeCongTermForwards thm thmType holeType
    unify hole (Data.Maybe.maybe id refl-term congTerm)


  rewriteOnceBackwards : Term → Term → TC ⊤
  rewriteOnceBackwards thm hole = do
    thmType <- inferType thm
    holeType <- inferType hole
    congTerm <- makeCongTermBackwards thm thmType holeType
    unify hole (Data.Maybe.maybe id refl-term congTerm)

  -- A version of the above that creates an transitive chain of replacesments
  -- using every equlity proof in the list.
  rewriteListOnce : List Term → Term → TC ⊤
  rewriteListOnce proofs hole = do
    holeType <- inferType hole
    congTerm <- makeCongTransChainForwards
                  proofs
                  holeType
                  refl-term
    unify hole congTerm

  rewriteAll : List Term → Term → TC ⊤
  rewriteAll proofs hole = do
    holeType <- inferType hole
    congTerm <- repeatMakeCongTransChainForwards
                  proofs
                  holeType
    unify hole congTerm
  -- Commented out as it does not work as expected with agdas type system
  -- autoCongAllTrans : Term → Term → TC ⊤
  -- autoCongAllTrans proofListTerm hole = do
  --   proofs <- listTerm→termList proofListTerm
  --   holeType <- inferType hole
  --   congTerm <- makeCongTransChainForwards
  --                 proofs
  --                 holeType
  --                 (con (quote refl) [])
  --   unify hole congTerm
  -- autoCongBegin : (Term → TC Term) → Term → TC ⊤
  -- autoCongBegin f hole = do
  --   sol <- f hole
  --   unify hole sol

  -- _andThen_ : Term → (Term → TC Term) → Term → TC Term
  -- _andThen_ proof cont goalType = do
  --   proofType <- inferType proof
  --   congTerm <- makeCongTermForwards proof proofType goalType
  --   congType <- inferType congTerm
  --   (if isYes (congType ≟ goalType)
  --    then returnTC congTerm
  --    else do
  --     congTypeRhs <- ≡-rhs congType
  --     newGoalType <- ≡-replace-lhs congTypeRhs goalType
  --     restTerm <- cont newGoalType
  --     returnTC (def (quote trans) ( arg (arg-info visible relevant) congTerm
  --                                 ∷ arg (arg-info visible relevant) restTerm
  --                                 ∷ [])))
