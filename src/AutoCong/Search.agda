{-
Author: Tomas Möre 2020
Email: tomas.o.more@gmail.com

This module defines functions that searches for an equivalence proofs.
Warning, may be slow.

There's currently several ways to improve this algorithm. The current
implementation here for evaluation.

Some possible improvements and extensions:
- Decide next step by some kind of similarity measure.
- In case no result is found: Keep track of list of results that are 'closest' to the goal.
- Heuristic: Use tree diff to only rewrite the parts of the goal that differs?
- Heuristic: Meet in the middle?
- Performanze: Create dynamic programming version

-}


module AutoCong.Search where

open import AutoCong.Utils.Abstraction
open import AutoCong.Utils.Terms
open import AutoCong.Utils.Utils

open import Function

open import Relation.Nullary.Decidable as Dec

open import Data.Product
open import Data.List using (List ; [] ; _∷_  ; [_] )
open import Data.Maybe using (Maybe ; just ; nothing ; _<∣>_ ; maybe)
open import Data.Bool using ( if_then_else_ )
open import Data.Unit using (⊤)
import Reflection.Term as Term

import Data.Nat as Nat
import Data.Nat.Show as Nat
import Data.String as String


open import Agda.Builtin.Nat
open import Agda.Builtin.Reflection
open import Agda.Builtin.Equality

-- Tbh, terrible name. Searches for a rewrite proof of equivalences given a list
-- of proofs. Efficently performs a DFS over the graph where the rewrites are
-- the edges. Will go to a max depth of the first argument.
{-# TERMINATING #-}
searchRewriteSolve : Nat → List (Term × (Type × Type)) → Term → Term → Maybe Term
searchRewriteSolve zero _ _ _ = nothing
searchRewriteSolve _ [] _ _ = nothing
searchRewriteSolve
  maxDepth proofs startType goal =
  rec maxDepth proofs startType refl-term
  where
    rec : Nat → List (Term × (Type × Type)) → Term → Term → Maybe Term
    recN : Nat → Nat → Term → (Type × Type) → Term → Term → Maybe Term
    recN zero _ _ _ _ _ = nothing
    recN (suc depth) nth proof proofType@(proofTypeLHS , proofTypeRHS) accType acc =
      case (abstractOnNth nth proofTypeLHS accType) of
        λ { nothing → nothing
          ; (just l) →
            let newAccType = betaReduce l proofTypeRHS
                congTerm = cong-term l proof
                newAcc = trans-term acc congTerm
                recurWithReplace =
                  rec depth proofs newAccType newAcc
                recurWithoutReplace =
                  recN (suc depth) (suc nth) proof proofType accType acc
            in recurWithReplace <∣> recurWithoutReplace
          }
    rec 0 _ _ _ = nothing
    rec _ [] accType acc =
      if isYes (accType Term.≟ goal)
      then just acc
      else nothing
    rec (suc depth) ((proof , ptLHS , ptRHS) ∷ proofs) accType acc =
      if isYes (accType Term.≟ goal)
      then just acc
      else maybe
             just
             (rec (suc depth) proofs accType acc)
             (recN depth 0 proof (ptLHS , ptRHS) accType acc)



-- incrementally pefroms 'searchRewriteSolve' at a starting depth. If it fails it re runs it with a maxDepth of startDepth + stepSize repeats 'maxIncrements' times
-- the goal type must be an equivalence, otherwise
incrementalSearchRewriteSolve : Nat → Nat → Nat → List (Term × (Type × Type)) → Term → Term → Maybe Term
incrementalSearchRewriteSolve _ _ _ [] _ _ = nothing
incrementalSearchRewriteSolve _ _ 0 _ _ _ = nothing
incrementalSearchRewriteSolve startDepth stepSize (suc maxIncrements) proofs startType goal  =
  maybe
    just
    (incrementalSearchRewriteSolve (startDepth + stepSize) stepSize maxIncrements proofs startType goal)
    (searchRewriteSolve startDepth proofs startType goal)





searchRewriteSolveM : Nat → List Term → Term → TC ⊤
searchRewriteSolveM 0 _ _ =
  typeError [ (strErr "searching with a depth of zero is no good...") ]
searchRewriteSolveM _ [] _ =
  typeError [ (strErr "searching with no rules is kinda pointles don't you think?") ]
searchRewriteSolveM maxDepth proofs hole = do
  (goalLhs , goalRhs) ← ≡-lhs×rhs hole
  proofTypeList ← ≡-proof-list proofs
  maybe
    (unify hole)
    (typeError [ strErr ("Unable to find a solution with depth " String.++ Nat.show maxDepth)])
    (searchRewriteSolve maxDepth proofTypeList goalLhs goalRhs)
  where
    open import Reflection using (_>>=_ ; _>>_)


incrementalSearchRewriteSolveM : Nat → Nat → Nat → List Term → Term → TC ⊤
incrementalSearchRewriteSolveM _ _ 0 _ _ =
  typeError [ strErr "Calling incremental search with zero depth isn't gonna help much..." ]
incrementalSearchRewriteSolveM _ _ _ [] _ =
  typeError [ (strErr "searching with no rules is kinda pointles don't you think?") ]
incrementalSearchRewriteSolveM startMaxDepth step iters proofs hole = do
  (goalLhs , goalRhs) ← ≡-lhs×rhs hole
  proofTypeList ← ≡-proof-list proofs
  maybe
    (unify hole)
    (typeError [ strErr ("Unable to find a solution with depth " String.++ Nat.show (startMaxDepth + step * iters) )])
    (incrementalSearchRewriteSolve startMaxDepth step iters proofTypeList goalLhs goalRhs)
  where
    open import Reflection using (_>>=_ ; _>>_)

macro
  -- Default rewrite search with an arbirary max depth
  rewriteSearch : List Term → Term → TC ⊤
  rewriteSearch = searchRewriteSolveM 128

  -- Rewrite search with a given max depth
  rewriteSearchN : Nat → List Term → Term → TC ⊤
  rewriteSearchN = searchRewriteSolveM

  -- Calls incremental search. First argument is starting max depth, second is the
  -- step and third is the max number of iterations.
  rewriteSearchIncremental : Nat → Nat → Nat → List Term → Term → TC ⊤
  rewriteSearchIncremental = incrementalSearchRewriteSolveM
