## AutoCong

Minor library that provides some functionality to automatically generate 'cong'
clauses for equational proofs. In other words it is a kind of `rewrite` tactic.
Developed with Agda v2.6.1 and agda-stdlib v1.3.

Created to learn about Agda's reflection system and solve an irritation I've had
when writing proofs.


Note that the macros exposed by this library aren't composable and are meant to
be easy to insert when doing equational reasoning. However, the functionalities
are not defined as macros and may be re-used in a composable tactics library.

Equality proofs are done by using the `cong` funciton.

Feedback, requests and ideas are most welcome!
Write an issue here or mail it to `tomas.o.more@gmail.com`.


Similar functionalities exists in UlfNorells Agda prelude
https://github.com/UlfNorell/agda-prelude/blob/4e0caf04617ebaa0c1a8af04128edd59c57bb4d3/test/Reright.agda

A composable core tactics library may be found here:
https://github.com/jespercockx/ataca. May implement integration with this library
in the future.



### Justification and examples
In many cases, Agdas builtin inference system in combination with the `with` and `rewrite` syntax works quite comfortably for smaller proofs. However, this method is not feasible for bigger proofs such as longer equational reasoning chains. To somewhat illustrate the point consider the following toy example:


``` agda
open import AutoCong.All
import Relation.Binary.PropositionalEquality as Eq
open Eq using (_≡_; refl; trans;)
open Eq.≡-Reasoning

postulate
  A : Set
  a : A
  b : A
  c : A
  op : A → A → A
  opr1 : op a b ≡ c
  opr2 : op c c ≡ a
  opr3 : op a c ≡ b
```
Here we postulate some things to create an imaginary problem of equational reasoning. Say now that we wish to prove the following statement `op (op (op (op c c) (op a c)) (op a b)) c ≡ b` this is easily provable using the postulated `opr#` proofs. Using the equational reasoning functions, we can for example create a proof like this:
``` agda
standardProof : op (op (op (op c c) (op a c)) (op a b)) c ≡ b
standardProof =
  begin
    op (op (op (op c c) (op a c)) (op a b)) c
  ≡⟨ cong (λ z → op (op (op z (op a c)) (op a b)) c)  opr2 ⟩
    op (op (op a (op a c)) (op a b)) c
  ≡⟨ cong (λ z →  op (op (op a z) (op a b)) c) opr3 ⟩
    op (op (op a b) (op a b)) c
  ≡⟨ cong (λ z →  op (op z z) c) opr1  ⟩
    op (op c c) c
  ≡⟨ cong (λ z → op z c)  opr2 ⟩
    op a c
  ≡⟨ opr3 ⟩
    b
  ∎
```
This style of proof is usually quite nice for both readability and for helping the writer to keep track of going on. But the required `cong` clauses cause a few possible pain points. Mainly, `cong` clasues can be a pain to write when the terms are large. But are also (subjectively) makes the proof harder to read. Therefor some automatic generation of such clauses may be nice to increase the enjoyment of writing such proofs. As en example we can generate all `cong` clauses in the above example using the `autoCongAll` function defined in this library like this:

``` agda
autoProof : op (op (op (op c c) (op a c)) (op a b)) c ≡ b
autoProof =
  begin
    op (op (op (op c c) (op a c)) (op a b)) c
  ≡⟨ rewriteOnce opr2 ⟩
    op (op (op a (op a c)) (op a b)) c
  ≡⟨ rewriteOnce opr3 ⟩
    op (op (op a b) (op a b)) c
  ≡⟨ rewriteOnce opr1 ⟩
    op (op c c) c
  ≡⟨ rewriteOnce opr2 ⟩
    op a c
  ≡⟨ opr3 ⟩
    b
  ∎
```
This version of  is subjectively both more readable and more fun to write.
If one does not care about having explicit equational reasoning then one can use `rewriteListOnce` which is the same thing.

``` agda
sillyEx2-trans-chain  : op (op (op (op c c) (op a c)) (op a b)) c ≡ b
sillyEx2-trans-chain =
  rewriteListOnce
    (  (quoteTerm opr2)
    ∷ (quoteTerm opr3)
    ∷ (quoteTerm opr1)
    ∷ (quoteTerm opr2)
    ∷ (quoteTerm opr3)
    ∷ [])

```
Sadly `quoteTerm` is required for each element in the list since agda would otherwise type-check the list. Will try to implement some kind of CPS style version of this later.

If one wishes to have explicit `cong` terms in the final result one can easily
get the computed results of these macros may be expanded in the Emacs agda mode by writing them inside holes and using the `C+c RET` command.

E.g:
``` agda
sillyEx1 : (op (op a b) (op a b)) ≡ (op c c)
sillyEx1 = {! rewriteOnce opr1 !}
```
expands to
``` agda
sillyEx1 : (op (op a b) (op a b)) ≡ (op c c)
sillyEx1 = cong (λ z₁ → op z₁ z₁) opr1
```

### Name terminology
#### Once
'Once' means that the rewrite will be applied once to the entire goal. I.e once
all terms have been identified it will not try to attempt the rewrite again.

#### Backwards
Means that the substitutions will be done using the right hand side of the goal
and the proof(s).


### Method overview

#### `rewriteOnce`
Takes an equality proof and a goal and replaces all occurrences of the left hand
side of the proof in the left hand side of the goal with the right hand side of
the proof.

In practice it construct a call to `cong` where the first argument is an version of the left hand side of the goal abstracted on the left hand side of the proof.

##### Example
``` agda
ex1 : (op (op a b) (op a b)) ≡ (op c c)
ex1 = rewriteOnce opr1
```

#### `rewriteOnceBackwards`
Like `rewriteOnce` but applies the substitution backwards. I.e replaces all
occurrences of the right hand side of the proof in the right hand side of the
goal with the left hand side of the proof.

May be useful in the process of writing proofs and some degree of backwards
proofs ease the problem.

##### Example
Typing `C+c C+a` in the last hole will yeield `op (op c c) c`
``` agda
ex2 : op (op (op (op c c) (op a c)) (op a b)) c ≡ op a c
ex2 =
  begin
    op (op (op (op c c) (op a c)) (op a b)) c
  ≡⟨ {!!} ⟩
    {!!}
  ≡⟨ rewriteOnceBackwards opr2 ⟩
    op a c
  ∎
```


#### `rewriteListOnce`

Creates a `trans` chain of cong calls using the provided list of
proofs. Essentially creates a chain of `rewriteOnce` calls. First proof in the
list is applied first.

- Proofs that aren't used is left out of the result.
- May terminate before applying all proofs if the goal is solved.

##### Example
``` agda
ex3  : op (op (op (op c c) (op a c)) (op a b)) c ≡ b
ex3 =
  rewriteListOnce
    ( (quoteTerm opr2)
    ∷ (quoteTerm opr3)
    ∷ (quoteTerm opr1)
    ∷ (quoteTerm opr2)
    ∷ (quoteTerm opr3)
    ∷ [])
```


#### `rewriteNth`
Replaces the n'th occurrence of the left hand side of the proof in the left hand
side of the goal. Occurrences are enumerated in pre order traversal of the syntax tree.
##### Example
``` agda
ex4 : (op (op a b) (op a b)) ≡ (op c (op a b))
ex4 = rewriteNth 0 opr1

ex5 : (op (op a b) (op a b)) ≡ (op (op a b) c)
ex5 = rewriteNth 1 opr1
```

#### `rewriteAll`
Uses the list of proofs to attempt to rewrite the left hand side of the goal. Recurs until the goal has been reached or no more substitutions may be done.

- May not yield the result you want as it does not do any kind of search.
- May not terminate
- If it finds a solution it will likely do so faster then more advanced searches

##### Example

``` agda
ex6  : op (op (op (op c c) (op a c)) (op a b)) c ≡ b
ex6 =
  rewriteAll
    ( (quoteTerm opr1)
    ∷ (quoteTerm opr2)
    ∷ (quoteTerm opr3)
    ∷ [])
```

#### `rewriteSearchN`
Searches for a solution to the equality goal using a user-provided list of
goals and a maximal search depth. If a solution exists at that depth this algorithm will find it. This is essentially a depth first search using the rules.

##### Example
``` agda
ex6 : (op (op a b) (op a b)) ≡ (op (op a b) c)
ex6 =
 rewriteSearchN 3
    ( (quoteTerm opr1)
    ∷ [])
```

#### `rewriteSearchIncremental`
A version of `rewriteSearchN` that incrementally increases its search depth when
a solution isn't found. This means that the chance of finding a solution in reasonable time increases as each depth will be completely explored before trying to go deeper. Could hugely benefit from some optimizations but should do OK for small - medium sized problems. (not tested)


##### Examples
in `ex6` the search would fail at n < 3.
``` agda
ex7 : (op (op a b) (op a b)) ≡ (op (op a b) c)
ex7  =
  rewriteSearchIncremental 1 1 1000
    ( (quoteTerm opr1)
    ∷ [])
```
### `rewriteSearch`

searches for a solution of the goal using a list of rules. May not terminate, uses a default search depth of `128` (completely arbitrary, may be changed in the future)




### Known problems / todo


#### Automatic rewrite proofs from context
Impossible to at the current moment (without invoking Haskell) since the full
context is not exposed inside the TC monad.

#### Tree matching performance
Tree matching is currently done naively with a `O(nm)` complexity when searching
for a syntax tree of size `n` in a syntax tree of size `m`. It should in fact be
possible to optimize this to `O(n+m)` using some subtree matching algorithms +
KMP.

(Such an algorithm may be slower for small cases.)

May be done in case the library gets real use.

#### Search performance
Performance may be greatly increased using a DP solution. Currently the same
proof branch may be explored multiple times.
